package retseptiRaamat;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import javafx.geometry.Insets;

public class RetseptiAknad extends Application {

    Stage window;
    Button breakfast;
    Button lunch;
    Button dinner;

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Retseptiraamat - JavaFX");
        breakfast = new Button("Hommikus��k");
        	breakfast.setOnAction(e-> breakfast_click());
        lunch = new Button("L�unas��k");
        	lunch.setOnAction(e-> lunch_click());
        dinner = new Button("�htus��k");
        	dinner.setOnAction(e-> dinner_click());
        

        HBox layout = new HBox();
        layout.setPadding(new Insets(15, 12, 15, 12));
        layout.setSpacing(10);
        layout.getChildren().addAll(breakfast, lunch, dinner);
        Scene scene = new Scene(layout);

        window.setScene(scene);
        window.show();
    }

public void breakfast_click() {
	Retsept.breakfast();
}

public void lunch_click() {
	Retsept.lunch();
}

public void dinner_click() {
	Retsept.dinner();
}
}

	


