package retseptiRaamat;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.stage.Stage;
import javafx.scene.layout.HBox;
import javafx.geometry.Insets;

public class TervitusAken extends RetseptiAknad {
	
	Stage window;
	Button enter;
	Button exit;
	
	public static void main(String[] args) {
        launch(args);
    }

	@Override
	
	public void start(Stage primaryStage) throws Exception {
        window = primaryStage;
        window.setTitle("Retseptiraamat - JavaFX");
        enter = new Button("Sisene");
        	enter.setOnAction(e-> enter_click());
        exit = new Button ("V�lju");
        
        HBox layout = new HBox();
        layout.setPadding(new Insets(15, 12, 15, 12));
        layout.setSpacing(10);
        layout.getChildren().addAll(enter, exit);
        Scene scene = new Scene(layout);

        window.setScene(scene);
        window.show();
	}
	
public void enter_click() {
	
	}

public void exit_click() {
	System.exit(0);
}
}