package retseptiRaamat;

public class Retsept extends RetseptiKogu {
	
	public Retsept() {
		
	}

	public static void breakfast() {
		System.out.println("Valisite hommikus��gi!");
		System.out.println("Kuna sa kunagi ei tea, mida sa s��a tahad, siis ma otsustan sinu eest!");		
		System.out.println("Valikus on puder, pannkoogid, omlett");

			int typeOfBreakfast = Retsept.random();				
				
				if (typeOfBreakfast == 1) {
					RetseptiKogu.mannapuder();					
				} else if (typeOfBreakfast == 2) {
					RetseptiKogu.pannkoogid();	
				} else if (typeOfBreakfast == 3) {
					RetseptiKogu.omlett();
				} else { 
					
			}
	}

	public static void lunch() {
		System.out.println("Valisite l�unas��gi!");	
		System.out.println("Kuna sa kunagi ei tea, mida sa s��a tahad, siis ma otsustan sinu eest!");		
		System.out.println("Valikus on: pasta, kanaroog, liharoog");
		
			int typeOfLunch = Retsept.random();
			
				if (typeOfLunch == 1) {
					RetseptiKogu.pasta();
				} else if (typeOfLunch == 2) {
					RetseptiKogu.kanaroog();
				} else if (typeOfLunch == 3) {
					RetseptiKogu.supp();
				}
	}
	
	public static void dinner() {
		System.out.println("Valisite �htus��gi!");
		System.out.println("Kuna sa kunagi ei tea, mida sa s��a tahad, siis ma otsustan sinu eest!");
		System.out.println("Valikus on: s�gavk�lmutatud pitsa, eilsed j��nused, n�lgi homseni");
		
			int typeOfDinner = Retsept.random();
			
				if (typeOfDinner == 1) {
					RetseptiKogu.pitsa();
				} else if (typeOfDinner == 2) {
					RetseptiKogu.j��nused();
				} else if (typeOfDinner == 3) {
					RetseptiKogu.n�lgid();
				}
				
	}
	public static void programStart () {	
		System.out.println("Tere tulemast Retseptiraamatusse!");
		System.out.println("Kui soovite eelnevalt kokku pandud retsepti, vajutage '1', kui soovite ise midagi kokku panna, vajutage '2'.");
			int valik1 = TextIO.getlnInt();
					if (valik1 == 1) {						
						Retsept.kokkuPandud();						
					} else if (valik1 == 2) {
						Retsept.iseTehtud();
					} else {
						System.out.println("Viga sisestusel! Tippige kas '1' v�i '2'");
						Retsept.programStart();
					}
	}	
	public static int random() {
		int max = 3;
		int min = 1;
		int random = (int )(Math.random() * max + min);
		return random;
	}
	
	public static void kokkuPandud() {
	System.out.println("Palun tippige, missugust rooga soovite valmistada.");
		System.out.println("hommikus��k, l�unas��k, �htus��k");
			String hommikus��k = new String("hommikus��k");
			String l�unas��k = new String("l�unas��k");
			String �htus��k = new String("�htus��k");
			String typeOfMeal = TextIO.getln();
			  
				if (typeOfMeal.equalsIgnoreCase(hommikus��k))   {
					Retsept hommikuRetsept = new Retsept();
					hommikuRetsept.breakfast();
					 
					
				} else if (typeOfMeal.equalsIgnoreCase(l�unas��k)) {
					Retsept l�unaRetsept = new Retsept ();
					l�unaRetsept.lunch();
					
					
				} else if (typeOfMeal.equalsIgnoreCase(�htus��k)) {
					Retsept �htuRetsept = new Retsept();
					�htuRetsept.dinner();
					
				
				} else {
					System.out.println("Viga sisestusel! Tippige kas 'hommikus��k', 'l�unas��k' v�i '�htus��k'");
					Retsept.kokkuPandud();
				}
	}		  	
				
	public static void iseTehtud() {
		System.out.println("Under development!");
		System.out.println("Vajuta 1, et minna tagasi!");
		int valik2 = TextIO.getlnInt();
		
			if (valik2 == 1) {
				Retsept.programStart();
			} else {
				Retsept.iseTehtud();
			}	
	}		
		

	public static void main(String[] args) {
		Retsept.programStart();
	}
 }

