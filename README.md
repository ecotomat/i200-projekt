# README #

Siin on minu I200 Java kodutöö projekt.
Programmi ülesanne on pakkuda kasutajale (antud juhul mulle) variante hommikusöögiks, lõunasöögiks ja õhtusöögiks.
Programm küsib, missuguse söögikorraga on tegemist, arvutab suvalise numbri ühest kolmeni, ja annab kasutajale retsepti määratud toidule.

Inspiratsioon minu tüdruksõbralt, kes ei suuda kunagi valida, mida ta süüa tahab.

Viited:
Math.Random() süntaks http://stackoverflow.com/questions/5887709/getting-random-numbers-in-java
Abimaterjalid aine kodulehelt