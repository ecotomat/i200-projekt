package retseptiRaamat;

public class RetseptiKogu {

	public RetseptiKogu() {		
	}


	
	
public static void mannapuder(){
	System.out.println("T�NA TEEME MANNAPUTRU!");
	System.out.println("Vaja l�heb: 1l piima, 1,5dl mannat, mett v�i suhkrut, n�put�is soola,");
	System.out.println("Kuumuta piim keemiseni. Klopi juurde manna. Hauta tasasel tulel umbes 10 minutit, aeg-ajalt segades.");
	System.out.println("Maitsesta puder n�puotsat�ie soola ja mee v�i suhkruga.");
}

public static void pannkoogid(){
	System.out.println("T�NA TEEME PANNKOOKE!");
	System.out.println("Vaja l�heb: 4 muna, 4sl suhkrut, 1l keefir v�i hapupiim, 1tl soola, 6dl jahu, 1tl s��gisooda");
	System.out.println("Eralda munakollased ja -valged. Klopi rebud suhkruga lahti, lisa keefir v�i hapupiim, sool ja v�hehaaval ka s��gisoodaga segatud jahu. Vahusta eraldi munavalged ning lisa tainale, t�sta ettevaatlikult segamini. K�pseta v�ikesed pannkoogid."); 
	System.out.println("Serveeri j��tise, moosi v�i suhkruga.");
}

public static void omlett(){
	System.out.println("T�NA TEEME OMLETTI!");
	System.out.println("Vaja l�heb: 2-3 muna, 2-3 sl vett, soola ja pipart");
	System.out.println("Klopi kausis munad, vesi ja maitseained lahti, aga mitte vahule");
	System.out.println("Kuumuta pannil v�i. Kui v�i on kergelt pruunistunud, lisa omletisegu.");
	System.out.println("Sega munamassi puulusikaga ��rtest keskele, nii et omlett �htlaselt h��biks");
	System.out.println("Voldi omlett pooleks ja libista taldrikule.");
}

public static void pasta(){
	System.out.println("T�NA TEEME PASTAT!");
	System.out.println("Vaja l�heb: Makaronid, tomatipasta, hakkliha, sibul");
	System.out.println("Keeda makaronid, prae hakkliha koos sibulaga, lisa tomatipasta hakklihale, serveeri koos");
}

public static void kanaroog(){
	System.out.println("T�NA TEEME KANAROOGA!");
	System.out.println("Vaja l�heb: Ahjukana");
	System.out.println("Soojenda ahi, pane kana ahju, hoia nii kaua kuni kana on pealt pruuniks l�inud.");
}

public static void supp(){
	System.out.println("T�NA TEEME SUPPI!");
	System.out.println("Osta poest seljanka, lisa keevale veele, maitsesta");
}

public static void pitsa() {
	System.out.println("JOPPAS SUL, T�NA TEEME PITSAT!");
	System.out.println("Vaja l�heb: s�gavk�lmutatud pitsa");
	System.out.println("Keera ahi 225C peale, eemalda pakend, sisesta pitsa ahju, oota 25 min");
	System.out.println("Parim nauding saavutatakse k�lma �llega");
}

public static void j��nused() {
	System.out.println("EI JOPAND! S��D SEDA, MIS EELMISTEST KORDADEST ALLES ON!");
}

public static void n�lgid() {
	System.out.println("N�lgid homseni. Ongi parem. Vaata ennast peeglist.");
}
public static void main(String[] args) {
		

	}

}
